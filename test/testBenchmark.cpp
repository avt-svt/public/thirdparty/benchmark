#include "benchmark/benchmark.h"
#include <set>
#include <vector>

static void BM_VectorInsert(benchmark::State &state) {
    std::vector<long> insertion_test;
  while (state.KeepRunning()) {

    for (long i = 0L, i_end = state.range(0L); i < i_end; i++) {
      insertion_test.push_back(i);
    }
  }
}

// Register the function as a benchmark
BENCHMARK(BM_VectorInsert)->Range(8, 8 << 10);

//~~~~~~~~~~~~~~~~

// Define another benchmark
static void BM_SetInsert(benchmark::State &state) {

  while (state.KeepRunning()) {
    std::set<long> insertion_test;
    for (long i = 0L, i_end = state.range(0L); i < i_end; i++) {
      insertion_test.insert(i);
    }
  }
}
BENCHMARK(BM_SetInsert)->Range(8, 8 << 10);

BENCHMARK_MAIN();
